export const errorLogin = (err, setErrorRedirect, changeLoader, setStatusLogin, changePassword) => {
  if (err.status === 401){
    changeLoader(false)
    setStatusLogin(err.data.message)
    setTimeout(() => {
      setStatusLogin('')
      changePassword('')
    }, 3000)
  }
  if (err.status === 500){
    setErrorRedirect(true)
  }
}