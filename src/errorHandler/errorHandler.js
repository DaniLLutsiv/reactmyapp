export const errorHandler = (err, state) => {
  if (err.status >= 500){
    setTimeout(() => state.setErrorRedirect(true),3000)
  }
  if (err.status === 401){
    localStorage.removeItem('token')
    state.changeLogin(false)
  }
}