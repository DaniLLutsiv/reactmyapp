import './App.css'
import {BrowserRouter, Route} from 'react-router-dom'
import LoginContainer from './component/Login/LoginContainer'
import CompleteMarkupContainer from './component/CompleteMarkup/CompleteMarkupContainer'
import ForgotPasswordContainer from './component/ForgotPassword/ForgotPasswordContainer'
import Oauth from './component/Oauth/Oauth'
import React from 'react'
import {Api} from './Api/Api'
import {MyProvider} from './component/MyProvider/MyProvider'
import MainContainer from './component/Main/MainContainer'
import Nav from './component/Nav/Nav'
import ProfileContainer from './component/Profile/ProfileContainer'
import InviteContainer from './component/Invite/InviteContainer'
import UsersContainer from './component/Users/UserContainer'
import Error from './component/Error/Error'
import {black} from './component/theme/black'
import {MuiThemeProvider} from '@material-ui/core'

const App = () => {
  return (
    <BrowserRouter>
      <MyProvider>
        <MuiThemeProvider theme={black}>
          <Route exact path="/" render={() => <><Nav/><MainContainer/></>}/>
          <Route path="/Login" render={() => <LoginContainer/>}/>
          <Route path="/CompleteRecover*" render={() => (
              <CompleteMarkupContainer Api={Api.apiCompleterecover} slice="38" buttonText="Change password"/>)}/>
          <Route path="/CompleteInvitation*" render={() => (
              <CompleteMarkupContainer Api={Api.apiCompleteInvitation} slice="41" buttonText="Add account"/>)}/>
          <Route path="/ForgotPassword" render={() => <ForgotPasswordContainer Api={Api.apiForgotPassword}/>}/>
          <Route path="/Oauth" render={() => <Oauth/>}/>
          <Route path="/Profile" render={() => <> <Nav/> <ProfileContainer/></>}/>
          <Route path="/Invite" render={() => <> <Nav/> <InviteContainer/></>}/>
          <Route path="/Users" render={() => <> <Nav/> <UsersContainer/></>}/>
          <Route path="/Error" render={() => <Error/>}/>
        </MuiThemeProvider>
      </MyProvider>
    </BrowserRouter>
  )
}

export default App