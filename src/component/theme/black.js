import {createMuiTheme} from '@material-ui/core'

export const black = createMuiTheme({
  palette: {
    primary: {
      light: '#1dd917', main: '#151515', dark: '#858585', contrastText: '#fff',
    },
    secondary: {
      light: '#ff7700',
      main: '#f50000',
      dark: '#f63737',
      contrastText: '#ffffff',
    },
  },
})