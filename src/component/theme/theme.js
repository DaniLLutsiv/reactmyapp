import {createMuiTheme} from '@material-ui/core'

export const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#1dd917', main: '#4CAF50', dark: '#1b72ac', contrastText: '#fff',
    },
    secondary: {
      light: '#6183ff',
      main: '#ff0000',
      dark: '#ff5c5c',
      contrastText: '#ffffff',
    },
  },
})