import React from 'react'

const styles={
  fontFamily: 'montserrat',
  fontSize: '20vw',
  fontWeight: 200,
  margin: 0,
  color: '#211b19',
  textTransform: 'uppercase',
  textAlign: 'center',
}

const stylesText = {
  position: 'absolute',
  fontSize: '5vw',
  backgroundColor: '#fff',
  top: '20%',
  left: '38%'
}
const Error = () => {
  return (
    <>
      <h1 style={styles}>Oops!</h1>
      <h2 style={stylesText}>Error 505</h2>
    </>
  )
}

export default Error