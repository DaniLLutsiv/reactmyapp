import React, {useContext, useState} from 'react'
import {MyContext} from '../MyProvider/MyProvider'
import Profile from './Profile'
import {Api} from '../../Api/Api'

const ProfileContainer = () => {
  const state = useContext(MyContext)
  const [nick, setNick] = useState('')
  const [random, setRandom] = useState(0)
  const [err, setErr] = useState(false)
  const [open, setOpen] = React.useState(false)
  const [open2, setOpen2] = React.useState(false)
  const [open3, setOpen3] = React.useState(false)

  const addLogo = (file) => {
    const formData = new FormData()
    formData.append('avatar', file)

    Api.apiSendLogo(formData, state.email).then((res) => {
      setRandom(Math.random)
    })
  }

  const addNick = () =>{
    Api.apiAddNick(nick).then((res) => {
      if (res.data.message === 'ok'){
        state.changeNick(nick)
      }else if (res.data.message === 'Small nick'){
        setErr(res.data.message)
        setTimeout(() => setErr(''), 3000)
      }
    })
  }

  return <Profile login={state.login} email={state.email} oauth={state.oauth}
                  addNick={addNick} setNick={setNick} nick={nick}
                  state={state} addLogo={addLogo} random={random}
                  err={err}
                  open={open} setOpen={setOpen}
                  open2={open2} setOpen2={setOpen2}
                  open3={open3} setOpen3={setOpen3}/>
}

export default ProfileContainer

