import React from 'react'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import AlternateEmailIcon from '@material-ui/icons/AlternateEmail'
import ListItemText from '@material-ui/core/ListItemText'
import ListItem from '@material-ui/core/ListItem'

const ProfileNick = (props) => {
  return (
      <>
        <ListItem button>
          <ListItemIcon>
            <AlternateEmailIcon/>
          </ListItemIcon>
          <ListItemText primary={'Email: ' + props.email}/>
        </ListItem>
      </>
  )
}

export default ProfileNick