import React from 'react'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import AddIcon from '@material-ui/icons/Add'
import ListItemText from '@material-ui/core/ListItemText'
import ExpandLess from '@material-ui/icons/ExpandLess'
import ExpandMore from '@material-ui/icons/ExpandMore'
import Collapse from '@material-ui/core/Collapse'
import List from '@material-ui/core/List'
import StarBorder from '@material-ui/icons/StarBorder'
import FacebookIcon from '@material-ui/icons/Facebook'
import {useStyles} from '../ProfileStyle'

const ProfileOauth = (props) => {
  const classes = useStyles()
  return (
  <>
    {(!props.oauth.google || !props.oauth.facebook) ?
    <>
      <ListItem button onClick={() => props.setOpen(!props.open)}>
        <ListItemIcon>
          <AddIcon/>
        </ListItemIcon>
        <ListItemText primary="Add oauth"/>
        {props.open ? <ExpandLess/> : <ExpandMore/>}
      </ListItem>

      <Collapse in={props.open} timeout="auto" unmountOnExit>
        <List component="div" disablePadding>

          {!props.oauth.google && <ListItemLink href={props.GOOGLELINK}>
            <ListItem button className={classes.nested}>
              <ListItemIcon>
                <StarBorder/>
              </ListItemIcon>
              <ListItemText primary="Google"/>
            </ListItem>
          </ListItemLink>}

          {!props.oauth.facebook && <ListItemLink href={props.FACEBOOKLINK}>
            <ListItem button className={classes.nested}>
              <ListItemIcon>
                <FacebookIcon/>
              </ListItemIcon>
              <ListItemText primary="Facebook"/>
            </ListItem>
          </ListItemLink>}

        </List>
      </Collapse>
    </> : false
    }
  </>
  )
}

function ListItemLink(props) {
  return <ListItem button component="a" {...props} />
}

export default ProfileOauth