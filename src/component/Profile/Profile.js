import React from 'react'
import {withAuthRedirect} from '../Hoc/withAuthRedirect'
import ListSubheader from '@material-ui/core/ListSubheader'
import List from '@material-ui/core/List'
import Box from '@material-ui/core/Box'
import Avatar from '@material-ui/core/Avatar'
import {useStyles} from './ProfileStyle'
import ProfileOauth from './ProfileElements/ProfileOauth'
import ProfileLogo from './ProfileElements/ProfileLogo'
import ProfileNick from './ProfileElements/ProfileNick'
import ProfileEmail from './ProfileElements/ProfileEmail'
import {Container} from '@material-ui/core'

const GOOGLELINK = 'https://accounts.google.com/o/oauth2/v2/auth?access_type=offline&prompt=consent&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fplus.me%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email&response_type=code&client_id=91799308541-4rn1glehaj5m4dfrkcu2bvjrfigabfpb.apps.googleusercontent.com&redirect_uri=http%3A%2F%2Flocalhost%3A3000%2FOauth'
const FACEBOOKLINK = 'https://www.facebook.com/v9.0/dialog/oauth?client_id=219506106203263&redirect_uri=http://localhost:3000/Oauth&scope=email'

const Profile = (props) => {
  const classes = useStyles()

  return (
      <Container maxWidth="md">
        <Box display="flex">
          <Box width={0.5} textAlign='center' m={5} mt={9}>
            <List className={classes.root} component="nav" aria-labelledby="nested-list-subheader"
                  subheader={
                    <ListSubheader component="div" id="nested-list-subheader">
                      Your actions
                    </ListSubheader>
                  }>

              <ProfileLogo setOpen3={props.setOpen3} open3={props.open3} addLogo={props.addLogo}/>

              <ProfileNick setOpen2={props.setOpen2} open2={props.open2} addNick={props.addNick}
                           nick={props.nick} err={props.err} setNick={props.setNick}/>

              <ProfileEmail email={props.email}/>

              <ProfileOauth open={props.open} GOOGLELINK={GOOGLELINK} FACEBOOKLINK={FACEBOOKLINK}
                            oauth={props.oauth} setOpen={props.setOpen}/>
            </List>
          </Box>

          <Box width={0.5} textAlign='center' m={5} mt={9}>
            <Avatar
                alt="Remy Sharp"
                src={"http://localhost:3015/avatars/"+props.state.email+ '?q=' + props.random}
                className={classes.large} />
            {props.state.nick &&
              <div className={classes.nick}>Nickname: {props.state.nick}</div>
            }
          </Box>
        </Box>
      </Container>
  )
}


export default withAuthRedirect(Profile)

