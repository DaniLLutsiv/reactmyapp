import React from 'react'
import AppBar from '@material-ui/core/AppBar'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import File from './File'
import Notes from './Notes'

const Actions = props => {
  return (
    <>
      <AppBar position="sticky">
        <Tabs value={props.selectedItem} onChange={props.onHandleChange} variant="fullWidth" aria-label="simple tabs example">
          <Tab label="File" fullWidth="250"/>
          <Tab label="Notes" />
        </Tabs>
      </AppBar>
      {props.selectedItem === 0 ? <File/>:<Notes/>}
    </>
  )
}
export default Actions