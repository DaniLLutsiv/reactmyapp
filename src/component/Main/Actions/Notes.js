import React, {useContext} from 'react'
import {ChatContext} from '../../MyProvider/ChatProvider'
import ChatMessage from '../Chat/Chunk/ChatMessage/ChatMessage'

const Notes = () => {
  const chatState = useContext(ChatContext)
  return <ChatMessage DataChat={chatState.dataChat.filter( el => el.notes)} changeNotes={chatState.changeNotes} actions={false}/>
}

export default Notes