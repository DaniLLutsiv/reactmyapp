import React, {useContext} from 'react'
import Chat from './Chat'
import {ChatContext} from '../../MyProvider/ChatProvider'
import {MyContext} from '../../MyProvider/MyProvider'

const ChatContainer = () =>{
  const chatState = useContext(ChatContext)
  const state = useContext(MyContext)

  return <Chat edit={chatState.edit} setEdit={chatState.setEdit} email={state.email} nick={state.nick}
               changeNotes={chatState.changeNotes} value={chatState.newMessage} change={chatState.changeNewMessage}
               changeDataChat={chatState.changeDataChat}
               DataChat={chatState.dataChat} users={chatState.users} changeSmsText={chatState.changeSmsText}/>
}
export default ChatContainer