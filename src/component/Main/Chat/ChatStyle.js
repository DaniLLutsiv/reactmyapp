import {makeStyles} from '@material-ui/core/styles'

export const useStyles = makeStyles({
  messageArea: {
    height: '70vh', overflowY: 'auto',
  },
})
