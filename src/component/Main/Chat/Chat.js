/*import React from 'react'
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import Box from '@material-ui/core/Box'
import ChatUsers from './Chunk/ChatUsers'
import ChatMessage from './Chunk/ChatMessage/ChatMessage'
import ChatInput from './Chunk/ChatInput'

const Chat = (props) => {
  return (
    <Box>
      <Grid container>
        <Grid item xs={12}>
          <Typography variant="h5" align="center">Online chat</Typography>
        </Grid>
      </Grid>
      <Grid container component={Paper} height='90vh'>
        <ChatUsers users={props.users}/>

        <ChatMessage DataChat={props.DataChat} email={props.email}
                     changeNotes={props.changeNotes} changeSmsText={props.changeSmsText}
                     setEdit={props.setEdit} actions={true}/>

        <ChatInput value={props.value} change={props.change}
                   changeDataChat={props.changeDataChat} email={props.email}
                   nick={props.nick} edit={props.edit}/>
      </Grid>
    </Box>
  )
}


export default Chat


*/
import React, {useEffect} from 'react'
import openSocket from 'socket.io-client'
import {useState} from "react"
//var socket = new WebSocket("ws://172.17.0.3:8080");
var socket = new WebSocket("wss://localhost:3000");
//const socket = openSocket('ws://localhost:8080')

function Chat() {
  const [value, setValue] = useState('')
  const [txt, setTxt] = useState([])

  /*socket.on('message', text => {
    console.log('message')
    setTxt( oldArray => [...oldArray, text])
  })*/
  useEffect(() => {
    debugger
      socket.onopen = function() {
        alert("Соединение установлено.");
    }

  })

  const click = () => {
    debugger
    socket.send('Hello');
  }
  return (
      <>
        <input value={value} onChange={(e) => setValue(e.target.value)}/>
        <button onClick={click}></button>
        {txt.map((html, index) => <div key={index}>{html}</div>)}
      </>
  )
}

export default Chat


