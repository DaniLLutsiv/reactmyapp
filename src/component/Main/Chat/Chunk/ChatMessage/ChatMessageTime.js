import React from 'react'
import Grid from '@material-ui/core/Grid'
import ListItemText from '@material-ui/core/ListItemText'

const ChatMessageTime = (props) => {
  return (
    <>
      <Grid item xs={12}>
        <div>
          <ListItemText align={props.author === props.email ? 'right' : 'left'} secondary={props.time}/>
        </div>
      </Grid>
    </>
  )
}

export default ChatMessageTime