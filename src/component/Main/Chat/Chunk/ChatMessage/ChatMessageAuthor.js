import React from 'react'
import Grid from '@material-ui/core/Grid'
import ListItemText from '@material-ui/core/ListItemText'

const ChatMessageAuthor = (props) => {
  /* позиция автора смс */
  return (
    <>
      {(props.author !== props.email) &&
          <Grid item xs={12}>
            <ListItemText align={props.author === props.email ? 'right' : 'left'}
                        secondary={props.nick ? props.nick : props.author}/>
          </Grid>
      }
    </>
  )
}

export default ChatMessageAuthor