import ListItem from '@material-ui/core/ListItem'
import React from 'react'
import Grid from '@material-ui/core/Grid'
import List from '@material-ui/core/List'
import Divider from '@material-ui/core/Divider'
import {useStyles} from '../../ChatStyle'
import ChatMessageAuthor from './ChatMessageAuthor'
import ChatMessageText from './ChatMessageText'
import ChatMessageTime from './ChatMessageTime'

const ChatMessage = (props) => {
  const classes = useStyles()
  return (
    <>
      <Grid item xs={10}>
        <List className={classes.messageArea}>
          {props.DataChat.map((sms) => (
            <ListItem key={sms.id}>
              <Grid container>
                <ChatMessageAuthor author={sms.author} email={props.email} nick={sms.nick}/>

                <ChatMessageText author={sms.author} email={props.email} message={sms.message}
                                 id={sms.id} notes={sms.notes} changeNotes={props.changeNotes}
                                 actions={props.actions} changeSmsText={props.changeSmsText}
                                 setEdit={props.setEdit}/>

                <ChatMessageTime author={sms.author} email={props.email} time={sms.time}/>
              </Grid>
            </ListItem>
          ))}
        </List>
      </Grid>
      <Divider/>
    </>
  )
}

export default ChatMessage