import React from 'react'
import Box from '@material-ui/core/Box'
import IconButton from '@material-ui/core/IconButton'
import LinkOffIcon from '@material-ui/icons/LinkOff'
import LinkIcon from '@material-ui/icons/Link'

const ChatMessageNotes = (props) => {
  if(props.notes) {
    return (
      <Box mt={1}>
        <IconButton color="primary" onClick={() => props.changeNotes(props.id)}><LinkOffIcon
            fontSize="small"/></IconButton>
      </Box>
    )
  }else{
    return (
      <Box mt={1}>
        <IconButton color="primary" onClick={() => props.changeNotes(props.id)}><LinkIcon fontSize="small"/></IconButton>
      </Box>
    )
  }
}

export default ChatMessageNotes