import React from 'react'
import Box from '@material-ui/core/Box'
import ChatMessageNotes from './ChatMessageNotes'
import ChatMessageEdit from './ChatMessageEdit'
import Grid from '@material-ui/core/Grid'

const ChatMessageText = (props) => {
  return (
    <>
      <Grid item xs={12}>
        <Box display="flex" flexDirection={props.author === props.email ? 'row-reverse' : 'row'}>
          <Box m={1} p={1.5} component="span" bgcolor='#f5f5f5' borderRadius='10px' maxWidth='150px' boxShadow={4}>
            <span>{props.message}</span>
          </Box>

          <ChatMessageNotes id={props.id} notes={props.notes} changeNotes={props.changeNotes}/>

          <ChatMessageEdit actions={props.actions} author={props.author} id={props.id}
                           email={props.email} changeSmsText={props.changeSmsText} setEdit={props.setEdit}/>
        </Box>
      </Grid>
    </>
  )
}

export default ChatMessageText