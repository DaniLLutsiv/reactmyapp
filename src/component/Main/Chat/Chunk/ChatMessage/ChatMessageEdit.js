import React from 'react'
import Box from '@material-ui/core/Box'
import EditIcon from '@material-ui/icons/Edit';
import IconButton from '@material-ui/core/IconButton'

const ChatMessageEdit = (props) => {
  const onClickEdit = () => {
    props.changeSmsText(props.id)
    props.setEdit(props.id)
  }
  return (
      <>
        {
          (props.actions && props.author === props.email) &&
            <Box mt={1}>
              <IconButton color="primary" onClick={onClickEdit}>
                <EditIcon fontSize="small" />
              </IconButton>
            </Box>
        }
    </>
  )
}

export default ChatMessageEdit