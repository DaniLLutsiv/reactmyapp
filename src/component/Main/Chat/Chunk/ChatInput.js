import React from 'react'
import Box from '@material-ui/core/Box'
import Grid from '@material-ui/core/Grid'
import Input from '../../../Elements/Input'
import Fab from '@material-ui/core/Fab'
import SendIcon from '@material-ui/icons/Send'

const ChatInput = (props) => {
  return (
    <>
      <Grid container style={{padding: '20px'}}>
        <Grid item xs={10}>
          <Box mr={4} ml={2}>
            <Input value={props.value} onChange={props.change}
                   type="" label="Type Something" id=""/>
          </Box>
        </Grid>
        <Grid item xs={1} align="right">
          <Fab color="primary" aria-label="add" onClick={() => props.changeDataChat(props.email, props.nick, props.edit)}><SendIcon/></Fab>
        </Grid>
      </Grid>
    </>
  )
}

export default ChatInput