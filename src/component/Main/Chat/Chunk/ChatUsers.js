import ListItem from '@material-ui/core/ListItem'
import Box from '@material-ui/core/Box'
import Avatar from '@material-ui/core/Avatar'
import Typography from '@material-ui/core/Typography'
import React from 'react'
import List from '@material-ui/core/List'
import Grid from '@material-ui/core/Grid'
import {useStyles} from '../ChatStyle'

const ChatUsers = (props) => {
  const classes = useStyles()

  return (
    <>
      <Grid item xs={2}>
        <Box borderRight="1px solid #e0e0e0" width="100px">
          <List className={classes.messageArea}>
            {props.users.map((el, index) => (<ListItem button key={index} width="100px">
              <Box mt={0.3} width="100px">
                <Box display="flex" justifyContent="center">
                  <Avatar alt="Remy Sharp" src={`https://material-ui.com/static/images/avatar/${index + 1}.jpg`}/>
                </Box>
                <Box display="flex" justifyContent="center">
                  <Typography variant="caption" align="center">{el}</Typography>
                </Box>
              </Box>
            </ListItem>))}
          </List>
        </Box>
      </Grid>
    </>
  )
}

  export default ChatUsers