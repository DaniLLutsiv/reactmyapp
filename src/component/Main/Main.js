import React from 'react'
import Box from '@material-ui/core/Box'
import Video from './Video/Video'
import ChatContainer from './Chat/ChatContainer'
import {redirectWithError} from '../Hoc/redirectWithError'
import Actions from './Actions/Actions'
import {ChatProvider} from '../MyProvider/ChatProvider'
import Grid from '@material-ui/core/Grid'

const Main = (props) => {
  return (
    <ChatProvider>
      <Box m={5} mt={9}>
        <Grid container spacing={5}>
          <Grid item xs={12} md={6}>
            <Video/>
            <Actions selectedItem={props.selectedItem} onHandleChange={props.onHandleChange}/>
          </Grid>
          <Grid item xs={12} md={6}>
            <ChatContainer/>
          </Grid>
        </Grid>
      </Box>
    </ChatProvider>
  )
}

export default redirectWithError(Main)