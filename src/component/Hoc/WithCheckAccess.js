import React, {useContext, useEffect, useState} from 'react'
import {Redirect} from 'react-router-dom'
import {MyContext} from '../MyProvider/MyProvider'
import {Api} from '../../Api/Api'

export const withAuthAccess = (Component) => ((props) => {
  const state = useContext(MyContext)
  const [redirect, ChangeRedirect] = useState('pending') // 'pending' or 'true' or 'false'

  useEffect(() => { /// если есть токен пошли проверочный запрос,
    if (localStorage.getItem('token')) {// если ты залогинен то "/" иначе отобрази компоненту
      Api.apiData()
          .then((data) => {
            if (data.data.access === 'admin') {
              state.changeAccess('admin')
            } else {
              state.changeAccess('user')
            }
            state.changeOauth(data.data.oauth)
            state.changeLogin(true)
            ChangeRedirect('true')
          })
          .catch(() => {
            state.changeLogin(false)
            localStorage.removeItem('token')
            ChangeRedirect('false')
          })
    } else {
      ChangeRedirect('false')
    }
  }, [])

  return <div>
    {redirect === 'pending' ? <></> : redirect === 'true' ? <Redirect to="/"/> : <Component {...props} />}
  </div>
})
