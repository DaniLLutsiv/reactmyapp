import React, {useContext} from 'react'
import {Redirect} from 'react-router-dom'
import {MyContext} from '../MyProvider/MyProvider'

export const withAuthRedirect = (Component) => ((props) => {
  const state = useContext(MyContext)
  if (!state.login) {
    return <Redirect to={`/login`}/>
  }
  return <Component {...props} />
})
