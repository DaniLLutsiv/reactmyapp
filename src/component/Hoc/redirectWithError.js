import React, {useContext} from 'react'
import {Redirect} from 'react-router-dom'
import {MyContext} from '../MyProvider/MyProvider'

export const redirectWithError = (Component) => ((props) => {
  const state = useContext(MyContext)
  if (state.errorRedirect) {
    return <Redirect to={`/Error`}/>
  }
  return <Component {...props} />
})
