import React, {useContext} from 'react'
import Drawer from '@material-ui/core/Drawer'
import Button from '@material-ui/core/Button'
import MenuIcon from '@material-ui/icons/Menu'
import Box from '@material-ui/core/Box'
import {MyContext} from '../MyProvider/MyProvider'
import NavList from './NavList'
import {useStyles} from './useStyles'

const Nav = () => {
  const classes = useStyles();
  const Mystate = useContext(MyContext)
  const [state, setState] = React.useState(false)

  return (
      <Box>
        <Box className={classes.nav}>
          <Button onClick={() => setState(true)}>
            <MenuIcon/>
          </Button>
          <Box mr={2}>
            <Button variant="contained" color="primary" onClick={Mystate.logout}>logout</Button>
          </Box>
        </Box>
        <Drawer anchor='left' open={state} onClose={() => setState(false)}>
          <NavList setState={setState} access={Mystate.access}/>
        </Drawer>
      </Box>
  )
}

export default Nav