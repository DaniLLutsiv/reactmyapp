import {NavLink} from 'react-router-dom'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import React from 'react'
import InboxIcon from '@material-ui/icons/MoveToInbox'
import MailIcon from '@material-ui/icons/Mail'

const ListNavPrivateItems = () => {
  const elements =
      ['Invite', 'Users'].map((text, index) => (
          <NavLink to={text} key={text}>
            <ListItem button>
              <ListItemIcon>
                {index % 2 === 0 ? <InboxIcon/> : <MailIcon/>}
              </ListItemIcon>
              <ListItemText primary={text}/>
            </ListItem>
          </NavLink>
      ))

  return (
      <>
        {elements}
      </>
  )
}
export default ListNavPrivateItems