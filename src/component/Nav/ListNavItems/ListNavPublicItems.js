import {NavLink} from 'react-router-dom'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import PeopleIcon from '@material-ui/icons/People'
import ListItemText from '@material-ui/core/ListItemText'
import React from 'react'

const ListNavPublicItems = () => {
  const elements =
      ['Profile'].map((text, index) => (
      <NavLink to={text} key={text}>
        <ListItem button>
          <ListItemIcon>
            <PeopleIcon/>
          </ListItemIcon>
          <ListItemText primary={text}/>
        </ListItem>
      </NavLink>))

  return (
      <>
        {elements}
      </>
  )
}
export default ListNavPublicItems