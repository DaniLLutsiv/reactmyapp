import makeStyles from '@material-ui/core/styles/makeStyles'
import {createStyles} from '@material-ui/core'

export const useStyles = makeStyles(() => createStyles({
  nav: {
    display: 'flex',
    justifyContent: 'space-between',
    position: 'fixed',
    width: '98%',
    backgroundColor: '#e7e7e7',
    top: '0',
    padding: '16px'
  },
}))