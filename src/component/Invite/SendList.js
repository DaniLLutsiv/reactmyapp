import React from 'react'
import InputError from '../Elements/InputError'
import Input from '../Elements/Input'
import Button from '@material-ui/core/Button'
import Box from '@material-ui/core/Box'
import Loader from '../Elements/Loader'

const SendList = (props) => {
  return <Box>
    {props.smsListen === 'error' ?
        <InputError helperText={props.text} label="Error" value={props.email}/> :
        <Input value={props.email} onChange={props.changeEmail} type="Required" label="Email"
               id="outlined-required"/>
    }
    <Box mt={2} display="flex">
      <Box mr={2} mt={1}>
        <Button variant="contained" color="primary" onClick={props.addNewPeople}>Add new people</Button>
      </Box>
      <Loader smsListen={props.smsListen}/>
    </Box>
  </Box>
}

export default SendList
