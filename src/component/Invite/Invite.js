import React from 'react'
import Table from '@material-ui/core/Table'
import TableContainer from '@material-ui/core/TableContainer'
import Paper from '@material-ui/core/Paper'
import {Box} from '@material-ui/core'
import {withAuthRedirect} from '../Hoc/withAuthRedirect'
import SendList from './SendList'
import InviteTableBody from './InviteElements/InviteTableBody'
import MyTeableHead from './InviteElements/MyTeableHead'
import {useStyles} from './InviteStyle'

function Invite(props) {
  const classes = useStyles()

  return (
    <div className={classes.wrap}>
      <Box m={5}>
        <SendList text={props.text} email={props.email} changeEmail={props.changeEmail}
                  addNewPeople={props.addNewPeople} smsListen={props.smsListen}/>
      </Box>
      <Box m={5}>
        <div className={classes.list}>
          <TableContainer component={Paper}>
            <Table className={classes.table} size="small" aria-label="a dense table">
              <MyTeableHead />
              <InviteTableBody collection={props.collection} resetEmail={props.resetEmail} reset={props.reset}/>
            </Table>
          </TableContainer>
        </div>
      </Box>
    </div>
  )
}
export default withAuthRedirect(Invite)