import React from 'react'
import Box from '@material-ui/core/Box'

const LoginStatus = (props) => {
  return (
    <Box m={2} fontSize={22} color="red">
      <div>{props.status}</div>
    </Box>
  )
}

export default LoginStatus