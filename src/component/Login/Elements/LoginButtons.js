import React from 'react'
import Button from '@material-ui/core/Button'
import {NavLink} from 'react-router-dom'
import Box from '@material-ui/core/Box'

const LoginButtons = (props) => {
  return (
      <Box m={1} p={1} display="flex" justifyContent='space-between'>
        <Button variant="contained" color="secondary" onClick={props.onVanishing}>Cancel</Button>
        <Button variant="contained" color="inherit">
          <NavLink to="/ForgotPassword">Forgot password?</NavLink>
        </Button>
      </Box>
  )
}
export default LoginButtons