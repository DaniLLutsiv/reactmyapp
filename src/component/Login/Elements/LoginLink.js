import React from 'react'
import CircularProgress from '@material-ui/core/CircularProgress'
import Box from '@material-ui/core/Box'
import {useStyles} from '../loginStyle'

const LINKFACEBOOK = 'https://www.facebook.com/v9.0/dialog/oauth?client_id=219506106203263&redirect_uri=http://localhost:3000/Oauth&scope=email'
const LINKGGOGLE = 'https://accounts.google.com/o/oauth2/v2/auth?access_type=offline&prompt=consent&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fplus.me%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email&response_type=code&client_id=91799308541-4rn1glehaj5m4dfrkcu2bvjrfigabfpb.apps.googleusercontent.com&redirect_uri=http%3A%2F%2Flocalhost%3A3000%2FOauth'

const LoginLink = (props) => {
  const classes = useStyles();
  return (
      <Box m={1} p={1} display="flex" justifyContent='space-between'>
        <a href={LINKGGOGLE}>
          <img src="http://localhost:3015/img/google.png" alt=""  className={classes.img}/>
        </a>

        {props.loader && <CircularProgress color="primary"/>}

        <a href={LINKFACEBOOK}>
          <img src="http://localhost:3015/img/facebook.png" alt=""  className={classes.img}/>
        </a>
      </Box>
  )
}

export default LoginLink