import React, {useContext, useState} from 'react'
import {Redirect} from 'react-router-dom'
import {Api} from '../../Api/Api'
import {MyContext} from '../MyProvider/MyProvider'
import {withAuthAccess} from '../Hoc/WithCheckAccess'
import {errorLogin} from '../../errorHandler/errorLogin'
import Login from './Login'

const LoginContainer = () => {
  const state = useContext(MyContext)
  const [email, changeEmail] = useState('')
  const [statusLogin, setStatusLogin] = useState('')
  const [password, changePassword] = useState('')
  const [loader, changeLoader] = useState(false)

  function onVanishing() {
    changeEmail('')
    changePassword('')
  }

  function set() {
    changeLoader(true)
    Api.login(email, password)
        .then(response => {
          changeLoader(false)
          if (response.data.message === 'Auth') {
            localStorage.setItem('token', response.data.token)
            state.changeLogin(true)
          }
        })
        .catch((reason) => {
          errorLogin(reason.response, state.setErrorRedirect, changeLoader, setStatusLogin, changePassword)
        })
  }

  return <>
    {state.login ?
      <Redirect to="/"/> :
      <Login onVanishing={onVanishing} changeEmail={changeEmail} changePassword={changePassword}
             set={set} status={state.status} statusLogin={statusLogin}
             email={email} password={password} loader={loader}/>}
  </>
}

export default withAuthAccess(LoginContainer)


