import React, {useContext, useState} from 'react'
import {Redirect} from 'react-router-dom'
import ForgotPassword from './ForgotPassword'
import setFP from './setFP'
import {withAuthAccess} from '../Hoc/WithCheckAccess'
import {MyContext} from '../MyProvider/MyProvider'

const ForgotPasswordContainer = (props) => {
  const state = useContext(MyContext)
  const [redirect, changeRedirect] = useState(false)
  const [statusForgot, setStatusForgot] = useState('')
  const [email, changeEmail] = useState('')
  const [smsListen, setSmsListen] = useState('') // '' or 'yes' or 'no' or 'error'

  const set = () => {
    setFP(email, props.Api, changeRedirect, setStatusForgot, setSmsListen, changeEmail, state)
  }

  return (
      <>
        {redirect ?
            <Redirect to="/login"/> :
            <ForgotPassword email={email} changeEmail={changeEmail} set={set}
                            statusForgot={statusForgot} smsListen={smsListen}/>}
      </>
  )

}

export default withAuthAccess(ForgotPasswordContainer)
