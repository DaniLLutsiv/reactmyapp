import {errorHandler} from '../../errorHandler/errorHandler'

const setFP = (email, Api, changeRedirect, setStatusForgot, setSmsListen, changeEmail, state) => {
    setSmsListen('yes')
    Api(email)
        .then((data) => {
          setTimeout(() => {setSmsListen(''); setStatusForgot('')}, 3000)
          if (data.data.message === 'ok') {
            setSmsListen('no')
            setStatusForgot(`Sms listen in email ${email}`)
            setTimeout(() => changeRedirect(true), 3000)
          }
        })
        .catch((reason) => {
          setTimeout(() => {setSmsListen(''); setStatusForgot('')}, 3000)
          setSmsListen('error')
          setStatusForgot(reason.response.data.message)
          changeEmail('')
          errorHandler(reason.response, state)
        })
}

export default setFP