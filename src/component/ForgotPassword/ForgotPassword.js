import React from 'react'
import Box from '@material-ui/core/Box'
import MyInputs from '../Elements/MyInputs'
import {redirectWithError} from '../Hoc/redirectWithError'
import MyButton from '../Elements/MyButton'
import {useStyles} from '../Login/loginStyle'
import MyHeaderForm from '../Elements/MyHeaderForm'
import ForgotStatus from './Elements/ForgotStatus'
import MyForm from '../Elements/MyForm/MyForm'

const ForgotPassword = (props) => {
  const classes = useStyles()
  const status = (props.statusForgot ? (!props.statusForgot.startsWith('sms') && true) : false )

  return (
   <MyForm>
     <MyHeaderForm text="Forgot password"/>
     <Box className={classes.form} noValidate>
       <MyInputs label='Email' id='outlined-required' type='Required' helperText={props.statusForgot}
                 value={props.email} onChange={props.changeEmail} status={status}/>

       <ForgotStatus smsListen={props.smsListen} statusForgot={props.statusForgot} smsListen={props.smsListen}/>

       <Box mt={2}>
         <MyButton view={props.email.length > 1} onSubmit={props.set} text='Send email'/>
       </Box>
     </Box>
   </MyForm>
  )
}

export default redirectWithError(ForgotPassword)