import React from 'react'
import Box from '@material-ui/core/Box'
import Loader from '../../Elements/Loader'

const ForgotStatus = (props) => {
  return (
    <>
      <Box display={props.smsListen ? 'flex' : 'none'} justifyContent='space-between' m={2}>
        {props.statusForgot.startsWith('sms') && props.statusForgot ?
            <Box mt={-0.5} color="green">
              <h3>{props.statusForgot}</h3>
            </Box>:
            <Box mt={-0.5} color="red">
              <h3>{props.statusForgot}</h3>
            </Box>
        }
        <Loader smsListen={props.smsListen}/>
      </Box>
    </>
  )
}

export default ForgotStatus