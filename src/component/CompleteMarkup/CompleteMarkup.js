import React from 'react'
import {Box} from '@material-ui/core'
import Loader from '../Elements/Loader'
import MyButton from '../Elements/MyButton'
import MyInputs from '../Elements/MyInputs'
import MyHeaderForm from '../Elements/MyHeaderForm'
import {useStyles} from '../Login/loginStyle'
import MyForm from '../Elements/MyForm/MyForm'

const CompleteMarkup = (props) => {
  const classes = useStyles();

  return (
      <MyForm>
        <MyHeaderForm text={props.buttonText}/>
        <Box className={classes.form} noValidate>
          <MyInputs label='Enter Password' id='outlined-password-input' type='password'
                    value={props.password1} onChange={props.changePassword1} status={props.statusInvitation}/>

          <MyInputs label='Repeat Password' id='outlined-password-input' type='password' helperText={props.statusInvitation}
                    value={props.password2} onChange={props.changePassword2} status={props.statusInvitation}/>

          <Loader smsListen={props.smsListen}/>
          <MyButton view={props.password1.length > 7 && props.password2.length > 7} text={props.buttonText} onSubmit={props.set}/>
        </Box>
      </MyForm>
  )
}

export default CompleteMarkup