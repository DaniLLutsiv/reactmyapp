import React, {useState} from 'react'
import {Redirect} from 'react-router-dom'
import CompleteMarkup from './CompleteMarkup'
import setCICR from './setCICR'
import {withAuthAccess} from '../Hoc/WithCheckAccess'

const CompleteMarkupContainer = (props) => { // рендер компоненты CompleteMarkupContainer или CompleteRecoverContainer
  const [redirect, changeRedirect] = useState(false) // bool
  const [statusInvitation, setStatusInvitation] = useState('') // text
  const [password1, changePassword1] = useState('') // text
  const [password2, changePassword2] = useState('') // text
  const [smsListen, setSmsListen] = useState('') // '' or 'yes' or 'no' or 'error'

  const set = () => {
      setCICR(password1, password2,
          props.slice, props.Api,
          changeRedirect, changePassword2,
          setStatusInvitation, setSmsListen)
  }

  return <>
    {redirect ?
        <Redirect to="/"/> :
        <CompleteMarkup password1={password1} password2={password2}
                        changePassword1={changePassword1} changePassword2={changePassword2}
                        set={set} smsListen={smsListen}
                        statusInvitation={statusInvitation} buttonText={props.buttonText}/>}
  </>

}

export default withAuthAccess(CompleteMarkupContainer)