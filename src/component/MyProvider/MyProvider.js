import React, {useState} from 'react'

export const MyContext = React.createContext()

export const MyProvider = ({children}) => {
  const [login, setLogin] = useState(false)
  const [errorRedirect, setErrorRedirect] = useState(false)
  const [email, setEmail] = useState(false)
  const [status, setStatus] = useState('')
  const [access, setAccess] = useState('')
  const [oauth, setOauth] = useState({google: false, facebook: false})
  const [nick, setNick] = useState('')
  return (
      <MyContext.Provider value={{
        login,
        status,
        access,
        oauth,
        email,
        nick,
        errorRedirect,
        setErrorRedirect,
        changeLogin(bool){
          setLogin(bool)
        },
        changeStatus(st){
          setStatus(st)
        },
        changeAccess(ac){
          setAccess(ac)
        },
        changeOauth(oa){
          setOauth(oa)
        },
        logout(){
          localStorage.removeItem('token')
          setLogin(false)
        },
        changeEmail(email){
          setEmail(email)
        },
        changeNick(n){
          setNick(n)
        }
      }}>
        {children}
      </MyContext.Provider>
  )
}