import React, {useState} from 'react'
export const ChatContext = React.createContext()

export const ChatProvider = ({children}) => {
  const [newMessage, setNewMessage] = useState('')
  const [edit, setEdit] = useState(null)
  const [dataChat, setDataChat] = useState([{
    id: 0, author: 'danil300914@gmail.com', nick: 'admin', time: '10:33', message: 'first message ssdvvdvsdsvsvvdv dsvsvsd sdvsdv!', notes: false,
  }, {
    id: 1, author: 'Maska@gmail.com', nick: 'Mashka', time: '10:34', message: 'love you', notes: false,
  }, {
    id: 2, author: 'danil300914@gmail.com', nick: 'admin', time: '10:35', message: 'I too!', notes: false,
  }, {
    id: 3, author: 'danil300914@gmail.com', nick: 'admin', time: '10:50', message: 'who you?', notes: false,
  }, {
    id: 4, author: 'den', nick: 'Denchik', time: '10:50', message: 'who you?', notes: true,
  }, {
    id: 5, author: 'danil300914@gmail.com', nick: 'admin', time: '10:50', message: 'who you?', notes: false,
  }])

  const [users, setUsers] = useState(['Den', 'Adolf228', 'Mashka', 'Nagibator', 'User'])
  return (<ChatContext.Provider value={{
    newMessage, dataChat, users, edit,
    changeNewMessage(txt) {
      setNewMessage(txt)
    },
    changeUsers(users) {
      setUsers(users)
    },
    changeDataChat(author, nick, id) {
      if (id !== null){
        setDataChat(prevState => {
          const newState = [...prevState]
          newState[id] = {
            id: id,
            author: newState[id].author,
            nick: newState[id].nick,
            time: newState[id].time,
            message: newMessage,
            notes: newState[id].notes,
          }
          return newState
        })
        setEdit(null)
      }else{
        setDataChat([...dataChat, {
          id: dataChat.length,
          author: author,
          nick: nick,
          time: `${new Date().getHours()}:${new Date().getMinutes() < 10 ? '0' + new Date().getMinutes():new Date().getMinutes()}`,
          message: newMessage,
          notes: false,
        }])
      }
      setNewMessage('')
    },
    changeNotes(id) {
      setDataChat(prevState => {
        const newState = [...prevState]
        newState[id] = {
          id: newState[id].id,
          author: newState[id].author,
          nick: newState[id].nick,
          time: newState[id].time,
          message: newState[id].message,
          notes: !newState[id].notes,
        }
        return newState
      })
    },
    changeSmsText(id){
      setNewMessage(dataChat[id].message)
    },
    setEdit
  }}>
    {children}
  </ChatContext.Provider>)
}