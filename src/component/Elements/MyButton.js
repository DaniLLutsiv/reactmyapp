import React from 'react'
import Button from '@material-ui/core/Button'
import Box from '@material-ui/core/Box'

const MyButton = (props) => {
  return (
      <Box m={2}>
        {/*props.value1.length > 0 && props.value2.length > 7 ?*/ props.view ?
            <Button fullWidth={true} variant="contained" color="primary" onClick={props.onSubmit}>{props.text}</Button> :
            <Button fullWidth={true} variant="contained" disabled>{props.text}</Button>}
      </Box>
  )
}

export default MyButton